//
//  MainViewCtrModel.h
//  DoNotRemember
//
//  Created by Jinniu on 2019/1/30.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BGFMDB.h"
NS_ASSUME_NONNULL_BEGIN

@interface MainViewCtrModel : NSObject
@property(nonatomic,copy)NSString * title;
@property(nonatomic,copy)NSString * tagStr;//所属标签
@property(nonatomic,copy)NSString * account;
@property(nonatomic,copy)NSString * password;
@property(nonatomic,copy)NSString * category;//所属分类
@property(nonatomic,copy)NSString * remark;
@end

NS_ASSUME_NONNULL_END
