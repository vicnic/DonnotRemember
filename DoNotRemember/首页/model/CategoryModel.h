//
//  CategoryModel.h
//  DoNotRemember
//
//  Created by Jinniu on 2019/1/30.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BGFMDB.h"
NS_ASSUME_NONNULL_BEGIN

@interface CategoryModel : NSObject
@property(nonatomic,copy)NSString * category;
@end

NS_ASSUME_NONNULL_END
