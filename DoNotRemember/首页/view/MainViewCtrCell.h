//
//  MainViewCtrCell.h
//  DoNotRemember
//
//  Created by Jinniu on 2019/1/30.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewCtrModel.h"

NS_ASSUME_NONNULL_BEGIN
typedef void (^AccountBlock)(void);
typedef void (^PasswordBlock)(void);
@interface MainViewCtrCell : UITableViewCell
@property(nonatomic,retain)MainViewCtrModel * model;
@property(nonatomic,copy)AccountBlock accountBlock;
@property(nonatomic,copy)PasswordBlock passBlock;
@end

NS_ASSUME_NONNULL_END
