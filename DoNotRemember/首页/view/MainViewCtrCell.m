//
//  MainViewCtrCell.m
//  DoNotRemember
//
//  Created by Jinniu on 2019/1/30.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "MainViewCtrCell.h"
@interface MainViewCtrCell ()
@property(nonatomic,strong)UILabel * titleLab, * tagLab,*createTimeLab;
@end
@implementation MainViewCtrCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createCusUI];
    }
    return self;
}
-(void)createCusUI{
    self.titleLab = [UILabel labelWithTitle:@"水电费水电费" color:[UIColor blackColor] font:[UIFont fontWithDevice:14]];
    [self.contentView addSubview:self.titleLab];
    _titleLab.sd_layout.leftSpaceToView(self.contentView, 10).topSpaceToView(self.contentView, 10).heightIs([self heightWithDevice:14]+1);
    [_titleLab setSingleLineAutoResizeWithMaxWidth:300];
    
    self.tagLab = [UILabel labelWithTitle:@"省的" color:[UIColor blackColor] font:[UIFont fontWithDevice:14]];
    [self.contentView addSubview:self.tagLab];
    _tagLab.sd_layout.rightSpaceToView(self.contentView, 10).centerYEqualToView(_titleLab).heightIs([self heightWithDevice:14]);
    [_tagLab setSingleLineAutoResizeWithMaxWidth:300];
    
    self.createTimeLab = [UILabel labelWithTitle:@"水电费是是是" color:[UIColor lightGrayColor] font:[UIFont fontWithDevice:12]];
    [self.contentView addSubview:self.createTimeLab];
    _createTimeLab.sd_layout.leftEqualToView(_titleLab).topSpaceToView(_titleLab, 10    ).heightIs([self heightWithDevice:12]);
    [_createTimeLab setSingleLineAutoResizeWithMaxWidth:300];
    
    
    UIButton * accountBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [accountBtn setTitle:@"复制账号" forState:UIControlStateNormal];
    [accountBtn addTarget:self action:@selector(accountBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [accountBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    accountBtn.titleLabel.font = [UIFont fontWithDevice:14];
    [self.contentView addSubview:accountBtn];
    accountBtn.sd_layout.leftEqualToView(self.contentView).bottomEqualToView(self.contentView).heightIs(40).widthIs(IPHONE_WIDTH/2.f);
    
    UIButton * passBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [passBtn setTitle:@"复制密码" forState:UIControlStateNormal];
    [passBtn addTarget:self action:@selector(passBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [passBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    passBtn.titleLabel.font = [UIFont fontWithDevice:14];
    [self.contentView addSubview:passBtn];
    passBtn.sd_layout.rightEqualToView(self.contentView).bottomEqualToView(self.contentView).heightIs(40).widthIs(IPHONE_WIDTH/2.f);
    
    UIView * vline = [UIView new];
    vline.backgroundColor = LineColor;
    [self.contentView addSubview:vline];
    vline.sd_layout.centerXEqualToView(self.contentView).heightIs(30).bottomSpaceToView(self.contentView, 5).widthIs(1);
    
}
-(void)setModel:(MainViewCtrModel *)model{
    _titleLab.text = model.title;
    _tagLab.text = [NSString stringWithFormat:@"#%@",model.tagStr];
    _createTimeLab.text = model.bg_updateTime;
}
-(void)accountBtnClick{
    if (self.accountBlock) {
        self.accountBlock();
    }
}
-(void)passBtnClick{
    if (self.passBlock) {
        self.passBlock();
    }
}
-(CGFloat)heightWithDevice:(CGFloat)height{
    if (IPHONE_WIDTH > 375) {
        height = height + 3;
    }else if (IPHONE_WIDTH == 375){
        height = height + 1.5;
    }else if (IPHONE_WIDTH == 320){
        height = height;
    }
    return height;
}
@end
