//
//  AddRecordCell.h
//  DoNotRemember
//
//  Created by Jinniu on 2019/1/30.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^InputBlock)(NSString * str);
NS_ASSUME_NONNULL_BEGIN

@interface AddRecordCell : UITableViewCell
@property(nonatomic,strong)UILabel * titleLab;
@property(nonatomic,strong)UITextField * inputTF;

@property(nonatomic,copy)InputBlock inputBlock;
@end

NS_ASSUME_NONNULL_END
