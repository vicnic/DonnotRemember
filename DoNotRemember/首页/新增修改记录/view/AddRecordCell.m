//
//  AddRecordCell.m
//  DoNotRemember
//
//  Created by Jinniu on 2019/1/30.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "AddRecordCell.h"
@interface AddRecordCell ()


@end
@implementation AddRecordCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createCusUI];
    }
    return self;
}
-(void)createCusUI{
    self.titleLab = [UILabel labelWithTitle:@"" color:[UIColor blackColor] font:[UIFont fontWithDevice:14]];
    [self.contentView addSubview:self.titleLab];
    _titleLab.sd_layout.leftSpaceToView(self.contentView, 15).centerYEqualToView(self.contentView).heightIs(16);
    [_titleLab setSingleLineAutoResizeWithMaxWidth:300];
    
    self.inputTF = [UITextField new];
    _inputTF.textAlignment = NSTextAlignmentRight;
    _inputTF.font = [UIFont fontWithDevice:14];
    _inputTF.placeholder = @"请输入内容";
    [_inputTF addTarget:self action:@selector(textFieldDidChangeValue:) forControlEvents:UIControlEventEditingChanged];
    [self.contentView addSubview:self.inputTF];
    _inputTF.sd_layout.leftSpaceToView(_titleLab, 10).rightSpaceToView(self.contentView, 10).centerYEqualToView(_titleLab).heightIs(40);
}
- (void)textFieldDidChangeValue:(UITextField*)textField{
    if (self.inputBlock) {
        self.inputBlock(textField.text);
    }
}
@end
