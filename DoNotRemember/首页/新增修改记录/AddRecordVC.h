//
//  AddRecordVC.h
//  DoNotRemember
//
//  Created by Jinniu on 2019/1/30.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "SuperViewController.h"
#import "MainViewCtrModel.h"
NS_ASSUME_NONNULL_BEGIN
typedef void(^RefreshBlock)(void);
@interface AddRecordVC : SuperViewController
@property(nonatomic,copy)RefreshBlock refreshBlock;
@property(nonatomic,retain)MainViewCtrModel * model;
@end

NS_ASSUME_NONNULL_END
