//
//  Mytools.h
//  7UGame
//
//  Created by 111 on 2017/2/14.
//  Copyright © 2017年 111. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef  enum{
    Common,//普通
    Success,//成功
    Toast,//吐司模式
    Error,//错误
}NoticeType;
@interface Mytools : NSObject
//验证URL
+(BOOL) verifyURL:(NSString *)url;

//用于MD5加密的封装方法
+(NSString *)getMD5code:(NSString *)pswStr;

//获取当前时间戳
+(NSString*)getCurrentTimestamp;

//字符串转日期
+(NSDate*)dateStrToDate:(NSString *)dateStr;

//日期转字符串
+(NSString *)dateToDateStr:(NSDate*)date;

//返回当天的日期
+(NSDate *)getCurrentDate;

//日期大小比较yyyy-MM-dd
+(int)compareOneDay:(NSDate *)oneDay withAnotherDay:(NSDate *)anotherDay;

+(NSString*)gotoWebView:(NSString * )str;

//url编码
+(NSString*)encodeString:(NSString*)unencodedString;

//时间戳转时间
+ (NSString *)timeStampTotime:(NSString *)timeString;

//创建虚线
+(void)drawDashLine:(UIView *)drawView dashCenter:(CGPoint)dashCenter startPoint:(CGPoint)sP endPoint:(CGPoint)eP lineWidth:(CGFloat)lineWidth;
//转化为金钱格式
+(NSString *)numberToMoneyFormatter:(long)num;

//sv消息弹窗
+(void)warnText:(NSString *)str status:(NoticeType)typeP;

+(void)deleteAllUserDefaules;

//取出字符串中的空格
+(NSString*)deleteSpaceInput:(NSString *)str;

//json格式字符串转字典
+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString;

//字典转json格式字符串
+ (NSString*)dictionaryToJson:(NSDictionary *)dic;

//数组转字符串
+ (NSString*)ArrtionaryToJson:(NSMutableArray *)arr;

//判断是不是整数
+(BOOL)isPureInt:(NSString*)string;
//判断是不是浮点数
+(BOOL)isPureFloat:(NSString*)string;
//验证是不是合法的(邮箱)
+(BOOL)isEmail:(NSString *)email;

//验证是不是合法的(身份证号码)
+(BOOL)isIdCardNum:(NSString *)IdCardNumber;

//验证是不是银行卡
+(BOOL)validateBankCardNumber:(NSString *)bankCardNumber;
+(BOOL)IsChinese:(NSString *)str ;
//字符串中获取数字
+(NSString *)getNumberForString:(NSString *)str;

//去除utc时间
+ (NSString *)htcTimeToLocationStr:(NSString*)strM;

//返回一个月多少天
+(NSInteger)howManyDaysInThisYear:(NSInteger)year withMonth:(NSInteger)month;


//取主色调
+(UIColor*)mostColor:(UIImage*)image;
+(NSDate*)dateStrToDate:(NSString *)dateStr withFormat:(NSString*)format;
+(NSString*)returnDecimalNumStr:(NSString*)numStr;//解决json解析精度丢失的问题

//date转成string（某年某月某日某时某分）
+(NSString*)dateTopString:(NSDate*)date;
//字符串转date
+(NSDate*)stringToDate:(NSString*)string;
//UIView转UIImage
+(UIImage*)viewBecomeImage:(UIView*)v;
//限制输入两位小数
+(NSString *)getMoneyFormat:(NSString*)money;
//判断推送通知是否开启
+ (BOOL)isUserNotificationEnable;
@end
