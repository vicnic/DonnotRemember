//
//  NSObject+ViewController.h
//  继承与NSObject的类
//
//  Created by mac on 16/4/25.
//  Copyright © 2016年 贝尔特伦. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface NSObject (ViewController)

- (UIViewController *)ViewController;



@end
