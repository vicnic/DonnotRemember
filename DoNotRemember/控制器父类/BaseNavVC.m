//
//  BaseNavVC.m
//  PaoBaoBao
//
//  Created by vicnic on 2017/10/8.
//  Copyright © 2017年 vicnic. All rights reserved.
//

#import "BaseNavVC.h"
#import "UIBarButtonItem+Extension.h"
@interface BaseNavVC ()<UINavigationControllerDelegate>

@end

@implementation BaseNavVC


//+ (void)initialize {
//
//    UINavigationBar *bar = [UINavigationBar appearance];
//    bar.translucent = NO;  //导航不透明
//    [bar setBarTintColor:ThemeColor];
//    NSMutableDictionary *attrs = [NSMutableDictionary dictionary];
//    attrs[NSFontAttributeName] = [UIFont systemFontOfSize:18.f];
//    attrs[NSForegroundColorAttributeName] = [UIColor blackColor];
//    [bar setTitleTextAttributes:attrs];
//
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.popDelegate = self.interactivePopGestureRecognizer.delegate;
    self.delegate = self;
    [[UINavigationBar appearance] setTintColor:[UIColor redColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor], NSFontAttributeName : [UIFont systemFontOfSize:18]}];
}
-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    if (self.childViewControllers.count>0) {
        viewController.hidesBottomBarWhenPushed = YES;
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:Image(@"导航返回键") forState:UIControlStateNormal];
        button.frame = CGRectMake(0, 0, kNavBarHeight, kNavBarHeight);
        // 让按钮内部的所有内容左对齐
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [button addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        // 修改导航栏左边的item
        viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        // 就有滑动返回功能
        self.interactivePopGestureRecognizer.delegate = nil;
    }
    [super pushViewController:viewController animated:animated];
}
- (void)back {
    [self popViewControllerAnimated:YES];
}
#pragma mark - 回到首页按钮
-(void)goBackHomeselect{
    [self popToRootViewControllerAnimated:YES];
    [self.tabBarController setSelectedIndex:0];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
