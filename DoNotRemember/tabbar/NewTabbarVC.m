//
//  NewTabbarVC.m
//  IDCardManager
//
//  Created by 金牛 on 2017/9/28.
//  Copyright © 2017年 zou. All rights reserved.
//

#import "NewTabbarVC.h"
#import "MainViewCtr.h"
#import "PersonVC.h"
#import "BaseNavVC.h"
@interface NewTabbarVC ()

@end

@implementation NewTabbarVC
- (void)viewDidLoad {
    [super viewDidLoad];
    //首页
    MainViewCtr * vc = [[MainViewCtr alloc]init];
    vc.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"首页" image:Image(@"球场") selectedImage:[self backImage:Image(@"球场-绿色")]];
    BaseNavVC * navVC = [[BaseNavVC alloc]initWithRootViewController:vc];

    //个人
    PersonVC * person  = [PersonVC new];
    person.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"个人" image:Image(@"个人") selectedImage:[self backImage:Image(@"个人-绿色")]];
    BaseNavVC * personNav = [[BaseNavVC alloc]initWithRootViewController:person];
    [[UITabBarItem appearance]setTitleTextAttributes:@{NSForegroundColorAttributeName:TabbarTitColor} forState:UIControlStateSelected];

    self.viewControllers = @[navVC,personNav];
    self.selectedIndex = 0;
}


-(UIImage*)backImage:(UIImage*)image{
    return  [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
