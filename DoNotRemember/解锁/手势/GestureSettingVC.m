//
//  GestureSettingVC.m
//  IDCardManager
//
//  Created by 金牛 on 2017/9/22.
//  Copyright © 2017年 zou. All rights reserved.
//

#import "GestureSettingVC.h"
#import "PCCircleView.h"
#import "PCLockLabel.h"
#import "PCCircleViewConst.h"
#import "PCCircleInfoView.h"
#import "PCCircle.h"
@interface GestureSettingVC ()<CircleViewDelegate>
{
    PCLockLabel * _msgLab ;
    PCCircleInfoView * _infoView;
    PCCircleView *_lockView;
    UIImageView * _bgView;
    UILabel * _navTitleLab;
    UIView * _bgUIView;
}
@end

@implementation GestureSettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"设置解锁手势";
    if (self.gestureType ==1) {
        [self createInputGestureView];
    }else{
        [self createSetGestureView];
    }
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // 禁用返回手势
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

-(void)createInputGestureView{
    _navTitleLab.text = @"输入解锁手势";
    _bgView = [[UIImageView alloc]initWithFrame:self.view.bounds];
    _bgView.userInteractionEnabled = YES;
    [self.view addSubview:_bgView];
    _bgView.image = Image(@"gesture_login");
    
    _lockView = [[PCCircleView alloc]initWithType:CircleViewTypeLogin clip:YES arrow:NO withCenterFloat:0];
    _lockView.delegate = self;
    [_bgView addSubview:_lockView];
    
    _msgLab = [[PCLockLabel alloc] init];
    _msgLab.frame = CGRectMake(0, 0, IPHONE_WIDTH, 14);
    _msgLab.center = CGPointMake(IPHONE_WIDTH/2, CGRectGetMinY(_lockView.frame) - 30);
    [_bgView addSubview:_msgLab];
}
-(void)createSetGestureView{
    _bgUIView = [[UIView alloc]initWithFrame:Frame(0, kTopHeight, IPHONE_WIDTH, IPHONE_HEIGHT-kTopHeight-KTabbarSafeBottomMargin)];
    [self.view addSubview:_bgUIView];
    
    _navTitleLab.text = @"设置解锁手势";
    self.view.backgroundColor = [UIColor colorWithHue:0.61 saturation:0.01 brightness:0.96 alpha:1];
    _lockView = [[PCCircleView alloc]initWithType:CircleViewTypeSetting clip:YES arrow:NO withCenterFloat:0];
    _lockView.delegate = self;
    [_bgUIView addSubview:_lockView];
    
    [_lockView setType:CircleViewTypeSetting];
    _msgLab = [[PCLockLabel alloc] init];
    _msgLab.frame = CGRectMake(0, 0, IPHONE_WIDTH, 14);
    _msgLab.center = CGPointMake(IPHONE_WIDTH/2, CGRectGetMinY(_lockView.frame) - 30);
    
    [_bgUIView addSubview:_msgLab];
    [_msgLab showNormalMsg:@"绘制解锁图案"];
    _infoView = [[PCCircleInfoView alloc] init];
    _infoView.frame = CGRectMake(0, 0, CircleRadius * 2 * 0.6, CircleRadius * 2 * 0.6);
    _infoView.center = CGPointMake(kScreenW/2, CGRectGetMinY(_msgLab.frame) - CGRectGetHeight(_infoView.frame)/2 - 10);
    [_bgUIView addSubview:_infoView];
    
    UIButton * forgetBtn = [UIButton new];
    [forgetBtn setTitle:@"重新绘制" forState:0];
    [forgetBtn setTitleColor:[UIColor blueColor] forState:0];
    [forgetBtn addTarget:self action:@selector(forgetBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [_bgUIView addSubview:forgetBtn];
    forgetBtn.sd_layout.centerXEqualToView(_bgUIView).bottomSpaceToView(_bgUIView, AUTO(10));
    [forgetBtn setupAutoSizeWithHorizontalPadding:10 buttonHeight:40];
}
- (void)circleView:(PCCircleView *)view type:(CircleViewType)type connectCirclesLessThanNeedWithGesture:(NSString *)gesture{
    NSString *gestureOne = [PCCircleViewConst getGestureWithKey:gestureOneSaveKey];
    // 看是否存在第一个密码
    if ([gestureOne length]) {
        VLog(@"提示再次绘制之前绘制的第一个手势密码");
        [_msgLab showNormalMsg:@"请重复正确手势"];
    } else {
        VLog(@"密码长度不合法%@", gesture);
        [_msgLab showWarnMsg:@"请设置至少四个连线"];
    }
}
- (void)circleView:(PCCircleView *)view type:(CircleViewType)type didCompleteSetFirstGesture:(NSString *)gesture{
    VLog(@"获得第一个手势密码%@", gesture);
    // infoView展示对应选中的圆
    [_msgLab showNormalMsg:@"请重复手势"];
    [self infoViewSelectedSubviewsSameAsCircleView:view];
    
}

- (void)circleView:(PCCircleView *)view type:(CircleViewType)type didCompleteSetSecondGesture:(NSString *)gesture result:(BOOL)equal{
    VLog(@"获得第二个手势密码%@",gesture);
    if (equal) {
        VLog(@"两次手势匹配！可以进行本地化保存了");
        NSUserDefaults * def = [NSUserDefaults standardUserDefaults];
        [def setObject:gesture forKey:@"gesturelock"];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.gesBackBlock(1);
            [self.navigationController popViewControllerAnimated:YES];
        });
        
    } else {
        VLog(@"两次手势不匹配！");
        [_msgLab showWarnMsg:@"两次手势不匹配！"];
//        dispatch_time_t time=dispatch_time(DISPATCH_TIME_NOW, 1*NSEC_PER_SEC);
//        dispatch_after(time, dispatch_get_main_queue(), ^{
//            dispatch_async(dispatch_get_main_queue(), ^{
//                self.gesBackBlock(2);
//                [self.navigationController popViewControllerAnimated:YES];
//            });
//        });
    }
}
#pragma mark - 让infoView对应按钮选中
- (void)infoViewSelectedSubviewsSameAsCircleView:(PCCircleView *)circleView {
    for (PCCircle *circle in circleView.subviews) {
        
        if (circle.circleState == CircleStateSelected || circle.circleState == CircleStateLastOneSelected) {
            
            for (PCCircle *infoCircle in _infoView.subviews) {
                if (infoCircle.tag == circle.tag) {
                    [infoCircle setCircleState:CircleStateSelected];
                }
            }
        }
    }
}

-(void)forgetBtnClick{
    [PCCircleViewConst saveGesture:nil Key:gestureOneSaveKey];
    [_msgLab showNormalMsg:@"绘制解锁图案"];
    for (PCCircle *circle in _lockView.subviews) {
        for (PCCircle *infoCircle in _infoView.subviews) {
            if (infoCircle.tag != circle.tag) {
                [infoCircle setCircleState:CircleStateNormal];
            }
        }
    }
}
-(void)backClick{
    _gesBackBlock(0);
    [self.navigationController popViewControllerAnimated:YES];
}

@end
