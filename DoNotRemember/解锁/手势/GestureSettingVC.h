//
//  GestureSettingVC.h
//  IDCardManager
//
//  Created by 金牛 on 2017/9/22.
//  Copyright © 2017年 zou. All rights reserved.
//

#import "SuperViewController.h"
typedef void (^GestureSetBackBlock)(NSInteger type);//type=1设置成功，2失败
@interface GestureSettingVC : SuperViewController
@property(nonatomic,assign)NSInteger gestureType;//根据本地内容得出是创建手势还是验证手势登录,0为创建手势，1为验证手势
@property(nonatomic,copy)GestureSetBackBlock gesBackBlock;
@end
