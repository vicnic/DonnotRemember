//
//  GestureLoginVC.h
//  IDCardManager
//
//  Created by 金牛 on 2017/8/26.
//  Copyright © 2017年 zou. All rights reserved.
//

#import "SuperViewController.h"

@interface GestureLoginVC : SuperViewController
@property(nonatomic,assign)NSInteger gestureType;//根据本地内容得出是创建手势还是验证手势登录,0为创建手势，1为验证手势
@end
