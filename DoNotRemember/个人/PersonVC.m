//
//  PersonVC.m
//  DoNotRemember
//
//  Created by Jinniu on 2019/1/30.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "PersonVC.h"
#import "CategoryMgrVC.h"
#import "GestureSettingVC.h"
#import "MainViewCtrModel.h"
@interface PersonVC ()<UITableViewDelegate,UITableViewDataSource>{
    NSArray * _titleArr;
}
@property(nonatomic,strong)UITableView * myTab;
@end

@implementation PersonVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"个人";
    _titleArr = @[@[@"分类管理"],@[@"手势解锁",@"指纹解锁"],@[@"导出全部"]];
    [self.view addSubview:self.myTab];
}
#pragma mark tableView代理
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _titleArr.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray * arr = _titleArr[section];
    return arr.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.text = _titleArr[indexPath.section][indexPath.row];
    cell.textLabel.font = [UIFont fontWithDevice:14];
    if (indexPath.section==1) {
        [self createGestureAndFingerByIndexPath:indexPath andCell:cell];
    }
    return cell;
}
-(void)createGestureAndFingerByIndexPath:(NSIndexPath*)indexPath andCell:(UITableViewCell*)cell{
    UISwitch *gestureSwitch = [[UISwitch alloc]initWithFrame:Frame(IPHONE_WIDTH-AUTO(70), (44-AUTO(30))/2.f, AUTO(20), AUTO(30))];
    gestureSwitch.transform = CGAffineTransformMakeScale(0.75, 0.75);
    gestureSwitch.onTintColor= [UIColor colorWithRed:0.09 green:0.47 blue:1 alpha:1];
    gestureSwitch.tag = indexPath.row + 10;
    NSUserDefaults * def = [NSUserDefaults standardUserDefaults];
    if (indexPath.row==0) {//@"手势解锁"
        if ([[def valueForKey:@"gesturestate"] isEqualToString:@"YES"]) {
            [gestureSwitch setOn:YES];
        }else if ([[def valueForKey:@"gesturestate"] isEqualToString:@"NO"]){
            [gestureSwitch setOn:NO];
        }
    }
    if (indexPath.row==1) {//@"指纹解锁"
        if (iPhoneX) {
            if ([[def valueForKey:@"faceidstate"] isEqualToString:@"YES"]) {
                [gestureSwitch setOn:YES];
            }else if ([[def valueForKey:@"faceidstate"] isEqualToString:@"NO"]){
                [gestureSwitch setOn:NO];
            }
        }else{
            if ([[def valueForKey:@"fingerstate"] isEqualToString:@"YES"]) {
                [gestureSwitch setOn:YES];
            }else if ([[def valueForKey:@"fingerstate"] isEqualToString:@"NO"]){
                [gestureSwitch setOn:NO];
            }
        }
        
    }
    [gestureSwitch addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    [cell.contentView addSubview:gestureSwitch];
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return  nil;
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section ==0 && indexPath.row ==0) {
        [self.navigationController pushViewController:[CategoryMgrVC new] animated:YES];
    }else if (indexPath.section ==2 && indexPath.row ==0){
        [self pasteAllData];
    }
}
-(void)pasteAllData{
    NSArray * arr = [MainViewCtrModel bg_findAll:MainViewCtrTab];
    [FTIndicator showProgressWithMessage:@"加载中"];
    NSString * finalStr = @"";
    for (MainViewCtrModel * mo in arr) {
        VLog(@"%@",finalStr);
        if (finalStr.length==0) {
            finalStr = [NSString stringWithFormat:@"标题：%@\n分类：%@\n标签：%@\n账号：%@\n密码：%@\n备注：%@\n",mo.title,mo.category,mo.tagStr,mo.account,mo.password,mo.remark];
        }else{
            finalStr = [NSString stringWithFormat:@"%@\n标题：%@\n分类：%@\n标签：%@\n账号：%@\n密码：%@\n备注：%@\n",finalStr,mo.title,mo.category,mo.tagStr,mo.account,mo.password,mo.remark];
        }
    }
    UIPasteboard * board = [UIPasteboard generalPasteboard];
    board.string = finalStr;
    [self performSelector:@selector(delayMethod) withObject:nil afterDelay:0.5];
}
-(void)delayMethod{
    [FTIndicator dismissProgress];
    [Mytools warnText:@"已导出到剪贴板，可以前往黏贴了" status:Success];
}

-(UITableView*)myTab{
    if (_myTab == nil) {
        _myTab = [[UITableView alloc]initWithFrame:CGRectMake(0, kTopHeight, IPHONE_WIDTH, IPHONE_HEIGHT-kTopHeight-kTabbarHeight) style:UITableViewStyleGrouped];
        _myTab.delegate = self;
        _myTab.dataSource = self;
    }
    return _myTab;
}
-(void)switchAction:(UISwitch *)sender{
    NSUserDefaults * def = [NSUserDefaults standardUserDefaults];
    UISwitch *switchButton = (UISwitch*)sender;
    BOOL isButtonOn = [switchButton isOn];
    if (sender.tag == 10) {//手势  打开手势解锁
        if (isButtonOn) {
            [switchButton setOn:YES];
            [def removeObjectForKey:@"gesturelock"];
            [def removeObjectForKey:@"gestureFinalSaveKey"];
            [def removeObjectForKey:@"gestureOneSaveKey"];
            GestureSettingVC * vc = [GestureSettingVC new];
            vc.gestureType = 0;
            vc.gesBackBlock = ^(NSInteger type) {
                if (type==1) {
                    [def setValue:@"YES" forKey:@"gesturestate"];
                }else{
                    [def setValue:@"NO" forKey:@"gesturestate"];
                    [switchButton setOn:NO];
                }
            };
            [self.navigationController pushViewController:vc animated:YES];

        }else {//@"关闭手势解锁"
            [switchButton setOn:NO];
            //修改手势缓存
            [def setValue:@"NO" forKey:@"gesturestate"];
        }
    }else if(sender.tag==11){//指纹或faceID
        if (iPhoneX) {
            if (isButtonOn) {//打开扫脸解锁
                [switchButton setOn:YES];
                //修改指纹缓存
                [def setValue:@"YES" forKey:@"faceidstate"];
            }else {//关闭扫脸解锁
                [switchButton setOn:NO];
                //修改指纹缓存
                [def setValue:@"NO" forKey:@"faceidstate"];
            }
        }else{
            if (isButtonOn) {//打开指纹解锁
                [switchButton setOn:YES];
                //修改指纹缓存
                [def setValue:@"YES" forKey:@"fingerstate"];
            }else {//关闭指纹解锁
                [switchButton setOn:NO];
                //修改指纹缓存
                [def setValue:@"NO" forKey:@"fingerstate"];
            }
        }
    }
}
@end
