//
//  CategoryMgrVC.m
//  DoNotRemember
//
//  Created by Jinniu on 2019/2/1.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "CategoryMgrVC.h"
#import "CategoryModel.h"
@interface CategoryMgrVC ()<UITableViewDelegate,UITableViewDataSource>{
    NSMutableArray * _dataArr;
}
@property(nonatomic,strong)UITableView * myTab;
@end

@implementation CategoryMgrVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"分类管理";
    [self createRightBtn];
    _dataArr = [NSMutableArray new];
    [self.view addSubview:self.myTab];
    [self getData];
}
-(void)getData{
    NSArray * arr = [CategoryModel bg_findAll:CategoryTab];
    [_dataArr addObjectsFromArray:arr];
    [_myTab reloadData];
}
-(void)createRightBtn{
    UIButton * rightBtn = [[UIButton alloc]initWithFrame:Frame(IPHONE_WIDTH-100, 0, 40, kNavBarHeight)];
    [rightBtn setImage:Image(@"选择按钮") forState:0];
    UIBarButtonItem * rightItme = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rightItme;
    [rightBtn addTarget:self action:@selector(rightNavBtnClick) forControlEvents:UIControlEventTouchUpInside];
}
-(void)rightNavBtnClick{
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"新增分类" message:@"输入你想添加的分类名称" preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        NSArray * textfields = alert.textFields;
        UITextField * passwordfiled = textfields[0];
        CategoryModel * mo  = [CategoryModel new];
        mo.bg_tableName = CategoryTab;
        mo.category = passwordfiled.text;
        [mo bg_save];
        [Mytools warnText:@"保存成功" status:Success];
        [self->_dataArr removeAllObjects];
        [self getData];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }]];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
    }];
    [self presentViewController:alert animated:YES completion:nil];
}
#pragma mark tableView代理
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataArr.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    CategoryModel * mo = _dataArr[indexPath.row];
    cell.textLabel.text = mo.category;
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.01;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return  nil;
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}
-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    return @"删除啦";
}
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    CategoryModel * mo = _dataArr[indexPath.row];
    [_dataArr removeObjectAtIndex:indexPath.row];
    NSString* where = [NSString stringWithFormat:@"where %@=%@",bg_sqlKey(bg_primaryKey),mo.bg_id];
    [CategoryModel bg_delete:CategoryTab where:where];
    [tableView reloadData];
}
-(UITableView*)myTab{
    if (_myTab == nil) {
        _myTab = [[UITableView alloc]initWithFrame:CGRectMake(0, kTopHeight, IPHONE_WIDTH, IPHONE_HEIGHT-kTopHeight) style:UITableViewStyleGrouped];
        _myTab.delegate = self;
        _myTab.dataSource = self;
    }
    return _myTab;
}
@end
