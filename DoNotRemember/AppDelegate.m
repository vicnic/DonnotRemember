//
//  AppDelegate.m
//  DoNotRemember
//
//  Created by Jinniu on 2019/1/30.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "AppDelegate.h"
#import "NewTabbarVC.h"
#import "IQKeyboardManager.h"
#import "FingerVerifyVC.h"
#import "GestureLoginVC.h"
#import "GestureSettingVC.h"
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = YES;//控制整个功能是否启用。
    manager.shouldResignOnTouchOutside = YES;//控制点击背景是否收起键盘
    manager.shouldShowToolbarPlaceholder = NO;//中间位置是否显示占位文字
    //设置占位文字的字体
    //    manager.placeholderFont = [UIFont boldSystemFontOfSize:17];
    manager.enableAutoToolbar = YES;//控制是否显示键盘上的工具条。
    NSUserDefaults * def = [NSUserDefaults standardUserDefaults];
    if (([def objectForKey:@"faceidstate"]==nil || [def objectForKey:@"fingerstate"]==nil)&& [def objectForKey:@"gesturestate"]==nil) {//第一次进入
        GestureSettingVC * vc = [GestureSettingVC new];
        vc.gestureType = 0;
        vc.gesBackBlock = ^(NSInteger type) {
            if (type==1) {
                [def setValue:@"YES" forKey:@"gesturestate"];
                self.window.rootViewController = [NewTabbarVC new];
            }else{
                [def setValue:@"NO" forKey:@"gesturestate"];
            }
        };
        self.window.rootViewController = vc;
    }else{
        if (([[def objectForKey:@"faceidstate"]isEqualToString:@"YES"]||[[def objectForKey:@"fingerstate"]isEqualToString:@"YES"])&&[[def objectForKey:@"gesturestate"]isEqualToString:@"NO"]) {
            self.window.rootViewController = [FingerVerifyVC new];
        }else{
            NSString * gesturType = [def objectForKey:@"gesturelock"];
            GestureLoginVC * ges = [GestureLoginVC new];
            if (gesturType.length !=0) {
                ges.gestureType = 1;
                ges.title = @"输入手势解锁";
            }else{
                ges.title = @"设置解锁图案";
                ges.gestureType = 0;
            }
            self.window.rootViewController = ges;
        }
    }
    
    return YES;
}


@end
